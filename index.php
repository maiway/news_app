<?php
	# include Class News, where extraction of data takes place
	include('class/News.class.php');
	# initialize News Class
	$news = new News;

	# n stands for news array. includes, page, data and title
	$n = null;
	# q is for parameters
	$q = isset($_GET['q']) ? $_GET['q'] : null;
	# nid is news id, this is use for viewing specific news
	$nid = isset($_GET['id']) ? $_GET['id'] : null;
	# create switch cases for the method
	switch ($q):
		case 'view':
			$n = $news->view($nid);
			break;
		case 'add':
			$n = $news->add();
			break;
		default:
			$n = $news->viewall();
			break;
	endswitch;
	
	# php page
	$page = $n['page'];
	# data from News Class
	$data = $n['data'];
	# page title
	$title = $n['title'];
	
	# include the template
	include('views/template.php');

 ?>