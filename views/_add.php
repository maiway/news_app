<h1><?=$title?></h1>
<hr>
<form method="post">
	<input type="hidden" name="token" value="<?=$_SESSION['token']?>">
	<div class="form-group">
		<label>Title</label>
		<input type="text" name="txttitle" class="form-control" required>
	</div>
	<div class="form-group">
		<label>Content</label>
		<textarea name="txtcontent" class="form-control" required></textarea>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
<hr>
<a href="index.php?q=viewall" class="link">View News</a>