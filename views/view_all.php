<h1><?=$title?></h1>
<a href="index.php?q=add" class="btn btn-sm btn-primary">Add News</a>
<hr>

<div class="row">
	<div class="col-9" <?=count($_SESSION['alert']) > 0 ? '' : 'hidden'?>>
		<div class="alert alert-<?=$_SESSION['alert'][0]?>">
			<?=$_SESSION['alert'][1]?>
		</div>
	</div>
</div>

<div class="row">
	<?php foreach($data as $news): ?>
		<div class="col-9" style="margin-bottom: 10px;">
			<div class="card">
				<div class="card-header">
					<b><a href="<?=htmlspecialchars('index.php?q=view&id='.$news['id'])?>" class="link"><b><?=htmlspecialchars($news['title'])?></b></a></b>
				</div>
				<div class="card-body"><?=htmlspecialchars($news['content'])?></div>
				<div class="card-footer"><small>Created Date: <?=htmlspecialchars(date('M. d, Y h:i A',strtotime($news['created_date'])))?></small></div>
			</div>
		</div>
	<?php endforeach; ?>
</div>