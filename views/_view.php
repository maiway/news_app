<h1><?=$data['title']?></h1>
<hr>
<div class="row">
	<div class="col-9" style="margin-bottom: 10px;">
		<div class="card">
			<div class="card-header"><b><?=htmlspecialchars($data['title'])?></b></div>
			<div class="card-body"><?=htmlspecialchars($data['content'])?></div>
			<div class="card-footer"><small>Created Date: <?=htmlspecialchars(date('M. d, Y h:i A',strtotime($data['created_date'])))?></small></div>
		</div>
	</div>
</div>
<a href="index.php?q=viewall" class="btn btn-sm btn-primary">View News List</a>