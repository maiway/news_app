<?php
include('class/Database.class.php');

class News
{
	public $db;
	function __construct() {
		session_start();
		$this->db = new Database();
	}

	# this function is for displaying all the data
	public function viewall()
	{
		# set session alert to empty if alert is more than 3 secs
		$_SESSION['alert'] = $_SESSION['created']+3 < time() ? array() : $_SESSION['alert'];
		# selecting all the news from database
		$news = $this->db->select('SELECT * FROM `news`');
		# return page to include in template, as well as parameters for views
		return array('page' => 'views/view_all.php', 'data' => $news, 'title' => 'News List');
	}

	# this function is for displaying specific news
	public function view($id)
	{
		# selecting news in database based on the id
		$news = $this->db->select('SELECT * FROM `news` WHERE id='.$id);
		# if news is empty it return null array, other wise, return the first data fetched.
		$news = count($news) > 0 ? $news[0] : array();
		# return page to include in template, as well as parameters for views
		return array('page' => 'views/_view.php', 'data' => $news, 'title' => 'View News');
	}

	# this function is for creating new news
	public function add()
	{
		# initialized message array, this is use for alert session
		$message = array();
		# check if post data is empty, 
		if(!empty($_POST)):
			# then session token and post token will compare, 
			if($_SESSION['token'] == $_POST['token']):
				# sets data to insert
				$raw = array('title'  => $_POST['txttitle'],
							'content' => $_POST['txtcontent']);
				# alert message, after submit
				$_SESSION['alert'] = $this->db->insert($raw,'news');
				$_SESSION['created'] = time();
				# then redirect to viewall page
				ob_start();
				header('Location: index.php?q=viewall');
				ob_end_flush();
			else:
				$_SESSION['alert'] = array('error','Invalid token');
			endif;
		else:
			# if session token is data, generate new token and set it to session token,
			if(empty($_SESSION['token'])):
				$_SESSION['token'] = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 32);
			endif;
		endif;
		return array('page' => 'views/_add.php', 'data' => $message, 'title' => 'Add News');
	}
	
}
 
?>