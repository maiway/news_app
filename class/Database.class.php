<?php

class Database
{
	# this function is for databse connection using PDO
	public function connect()
	{
		$settings = $this->extract_dbfile();
		try{
			# create connection
			$conn = new PDO($settings['connection'].':host='.$settings['host'].';dbname='.$settings['dbase'],$settings['username'],$settings['pword']);
			# set the PDO error mode to exeption
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;
		}
		catch(PDOException $e)
		{
			# echo exception error message
			echo $e->getMessage();
		}
	}

	# this function is for insert row in database
	public function insert($data,$tablename)
	{
		$conn = $this->connect();
		try{
			# prepare statement
			$chain = rtrim(str_repeat('?,',count(array_values($data))),',');
			# sql statement for insert
			$sql = "INSERT INTO ".$tablename." (".implode(',',array_keys($data)).") VALUES (".$chain.")";
			$stmt = $conn->prepare($sql);
			$res = $stmt->execute(array_values($data));
			if($res) {
				return array('success','New Record has been added.');
			} else {
				return array('error','Error: '.$sql.'<br>'.$conn->error());
			}
		}
		catch(Exception $e)
		{
			# echo exception error message
			echo $e->getMessage();
		}
	}

	# this function is for insert row in database
	public function select($sql)
	{
		$conn = $this->connect();
		try{
			# set the prepared statement;
			return $conn->query($sql)->fetchAll();
		}
		catch(Exception $e)
		{
			# echo exception error message
			echo $e->getMessage();
		}
	}

	# this function is for extracting file from settings inc
	public function extract_dbfile()
	{
		$settings = array();
		$dbfile = fopen('settings.inc','r');
		while (! feof($dbfile)) {
			$line = fgets($dbfile);
			# get connection
			$host = 'DB_CONNECTION';
			if(substr($line,0,strlen($host)) === $host) {
				$settings['connection'] = trim(substr($line,strlen($host)+1));
			}
			# get host
			$host = 'DB_HOST';
			if(substr($line,0,strlen($host)) === $host) {
				$settings['host'] = trim(substr($line,strlen($host)+1));
			}
			# get dbname
			$dbname = 'DB_DATABASE';
			if(substr($line,0,strlen($dbname)) === $dbname) {
				$settings['dbase'] = trim(substr($line,strlen($dbname)+1));
			}
			# get username
			$username = 'DB_USERNAME';
			if(substr($line,0,strlen($username)) === $username) {
				$settings['username'] = trim(substr($line,strlen($username)+1));
			}
			# get password
			$pword = 'DB_PASSWORD';
			if(substr($line,0,strlen($pword)) === $pword) {
				$settings['pword'] = trim(substr($line,strlen($pword)+1));
			}
		}
		return $settings;
	}

}

?>